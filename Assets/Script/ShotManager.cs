﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotManager : SingletonMonoBhv<ShotManager>
{
    public void CreateShot(Entity.ShotData shotData, CharacterView view, GameObject targetUnit)
    {
        switch (shotData._Type)
        {
            case Entity.Constant.ShotType.kNormal:
                CreateNormalShot(shotData, view, targetUnit);
                break;

            case Entity.Constant.ShotType.kNormalPush:
                CreateNormalShot(shotData, view, targetUnit);
                break;

            case Entity.Constant.ShotType.kWide:
                Entity.WideShotData wideData = (Entity.WideShotData)shotData;
                CreateWideShot(wideData, view, targetUnit);
                break;

            case Entity.Constant.ShotType.kHoming:
                Entity.HomingShotData homingData = (Entity.HomingShotData)shotData;
                CreateHomingShot(homingData, view, targetUnit);
                break;

            case Entity.Constant.ShotType.kBomb:
                break;
        }
    }

    private void CreateNormalShot(Entity.ShotData shotData, CharacterView view, GameObject targetUnit)
    {
        GameObject shotObj = Instantiate(shotData._Prafab, view.ShotPos.position, view.ShotPos.rotation);
        ShotPresenter shotPre = shotObj.GetComponentInChildren<ShotPresenter>();
        shotPre.Initialize(shotData, targetUnit, shotObj.transform.localPosition);
    }

    /// <summary>
    /// ワイドショットを作成します。
    /// </summary>
    /// <param name="shotData"></param>
    /// <param name="view"></param>
    /// <param name="targetUnit"></param>
    private void CreateWideShot(Entity.WideShotData shotData, CharacterView view, GameObject targetUnit)
    {
        GameObject shotObj = Instantiate(shotData._Prafab, view.ShotPos.position, view.ShotPos.rotation);
        ShotPresenter shotPre = shotObj.GetComponentInChildren<ShotPresenter>();

        shotPre.Initialize(shotData, targetUnit, shotObj.transform.localPosition);

        for (int i = 0; i < shotData._WayCount / 2; i++)
        {
            float addRot = shotData._AddRot * (i + 1);

            shotObj = Instantiate(shotData._Prafab, view.ShotPos.position, view.ShotPos.rotation);
            shotObj.transform.Rotate(new Vector3(0, 0, addRot));
            shotPre = shotObj.GetComponentInChildren<ShotPresenter>();
            shotPre.InitializeWide(shotData, targetUnit, shotObj.transform.localPosition, addRot);

            shotObj = Instantiate(shotData._Prafab, view.ShotPos.position, view.ShotPos.rotation);
            shotObj.transform.Rotate(new Vector3(0, 0, -addRot));
            shotPre = shotObj.GetComponentInChildren<ShotPresenter>();
            shotPre.InitializeWide(shotData, targetUnit, shotObj.transform.localPosition, -addRot);
        }
    }

    private void CreateHomingShot(Entity.HomingShotData shotData, CharacterView view, GameObject targetUnit)
    {
        GameObject shotObj = Instantiate(shotData._Prafab, view.ShotPos.position, view.ShotPos.rotation);
        ShotPresenter shotPre = shotObj.GetComponentInChildren<ShotPresenter>();
        shotPre.InitializeHoming(shotData, targetUnit, shotObj.transform.localPosition);

    }
}
