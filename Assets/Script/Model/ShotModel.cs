﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using System;

public class ShotModel : MonoBehaviour
{
    [Header("デバッグ表示")]
    [SerializeField] private Entity.ShotData _ShotData = default;
    public Entity.ShotData ShotData { get { return _ShotData; } }

    private GameObject _TargetUnit = default;
    public GameObject TargetUnit { get { return _TargetUnit; } }

    public string TargetTag {
        get { return _TargetUnit.tag; }
    }

    #region Position
    private Vector2 _LocalPos = default;
    private Vector2ReactiveProperty _RxLocalPos = default;
    public IObservable<Vector2> LocalPosObservable { get { return _RxLocalPos.AsObservable(); } }
    public Vector2 LocalPos
    {
        get { return _RxLocalPos.Value; }
        private set { _RxLocalPos.Value = value; }
    }
    public void UpdatLocalPos()
    {
        _RxLocalPos.Value += (AddPos * _ShotData._MoveSpeed * Time.deltaTime);
    }
    #endregion

    #region 加算値
    [SerializeField]private Vector2 _AddPos;
    public Vector2 AddPos { get { return _AddPos; } }

    [SerializeField] private float _BackupRotDeg = 0.0f;

    public void SetRot(Vector2 targetPos)
    {
        Vector2 distance = targetPos - _LocalPos;
        float rad = Mathf.Atan2(distance.y, distance.x);
        _AddPos.x = Mathf.Cos(rad);
        _AddPos.y = Mathf.Sin(rad);
        _BackupRotDeg = rad * Mathf.Rad2Deg; 
    }

    public void SetRotWide(Vector2 targetPos, float addRot)
    {
        Vector2 distance = targetPos - _LocalPos;
        float rad = Mathf.Atan2(distance.y, distance.x) + (float)(addRot * Math.PI / 180);
        _AddPos.x = Mathf.Cos(rad);
        _AddPos.y = Mathf.Sin(rad);
        _BackupRotDeg = rad * Mathf.Rad2Deg;
    }

    public void SetRotHoming(Transform targetTransform, float addRotDeg, Transform viewTranform)
    {
        // 弾と対称の角度を取得
        Vector2 targetPos = targetTransform.localPosition;
        Vector2 distance = targetPos - _LocalPos;
        float rotDeg = Mathf.Atan2(distance.y, distance.x) * Mathf.Rad2Deg;
        //LogView.Log("rotDeg : " + rotDeg.ToString());

        // 角度差
        var deltaAngle = Mathf.DeltaAngle(_BackupRotDeg, rotDeg);
        if (Mathf.Abs(deltaAngle) < addRotDeg)
        {
        }
        else if (deltaAngle > 0)
        {
            rotDeg += addRotDeg;
            _BackupRotDeg += addRotDeg * Mathf.Deg2Rad;
        }
        else
        {
            rotDeg -= addRotDeg;
            _BackupRotDeg -= addRotDeg * Mathf.Deg2Rad;
        }

        _AddPos.x = Mathf.Cos(rotDeg * Mathf.Deg2Rad);
        _AddPos.y = Mathf.Sin(rotDeg * Mathf.Deg2Rad);
    }
    #endregion

    public void Initialize(Entity.ShotData shotData, GameObject targetUnit, Vector2 localPos)
    {
        _ShotData = shotData;
        _LocalPos = localPos;
        _TargetUnit = targetUnit;
        _RxLocalPos = new Vector2ReactiveProperty(_LocalPos);
    }
}
