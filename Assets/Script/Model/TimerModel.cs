﻿using UniRx;
using UnityEngine;

public class TimerModel
{
    private int _Interval = 1;
    public int Interval
    {
        get { return _Interval; }
        set { _Interval = value; }
    }

    private IntReactiveProperty _Second;
    public IntReactiveProperty Second
    {
        get { return _Second; }
    }

    public enum TimerStateType { Idle, Loop };
    private TimerStateType _TimerState = TimerStateType.Idle;
    public TimerStateType TimerState
    {
        get { return _TimerState; }
        set { _TimerState = value; }
    }

    public void Initialize(int time, GameObject parent)
    {
        _Second = new IntReactiveProperty(time);
        _TimerState = TimerStateType.Loop;
    }
}
