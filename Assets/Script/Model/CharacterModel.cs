﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class CharacterModel
{
    private Entity.Constant.PlayerType _PlayerType = Entity.Constant.PlayerType.kAI;
    private Entity.CharacterData _Character = default;

    private GameObject _TargetUnit = default;
    public GameObject TargetUnit { get { return _TargetUnit; } }

    public bool IsPlayer => _PlayerType == Entity.Constant.PlayerType.kUser;

    #region Gage
    private int _MaxGage = default;

    private IntReactiveProperty _RxShotGage = default;
    public IntReactiveProperty RxShotGage { get { return _RxShotGage; } }

    public int ShotGage
    {

        get { return _RxShotGage.Value; }
        private set { _RxShotGage.Value = value; }
    }
    #endregion

    #region HP
    private int _MaxHP = default;
    public int MaxHP { get { return _MaxHP; } }

    private IntReactiveProperty _RxHP = default;
    public IntReactiveProperty RxHP { get { return _RxHP; } }

    public int HP
    {
        get { return _RxHP.Value; }
        private set { _RxHP.Value = value; }
    }
    #endregion

    #region position
    private Vector2 _InitialLocalPos = default;
    private Vector2 _LatestLocalPos = default;

    private Vector2ReactiveProperty _RxLocalPos = default;
    public IObservable<Vector2> LocalPosObservable { get { return _RxLocalPos.AsObservable(); } }

    public Vector2 LocalPos
    {
        get { return _RxLocalPos.Value; }
        private set
        {
            _LatestLocalPos = _RxLocalPos.Value;
            {
                _RxLocalPos.Value = new Vector2(
                    Mathf.Clamp(value.x, -2.2f, 2.2f),
                    Mathf.Clamp(value.y, -1.05f, 2.6f));
            }
        }
    }

    public Vector2 LatestLocalPos
    {
        get { return _LatestLocalPos; }
        set { _LatestLocalPos = value; }
    }
    #endregion

    public void Initialize(Entity.Constant.PlayerType playerType, Entity.CharacterData character, Vector2 localPos, GameObject targetUnit)
    {
        //LogView.Log("CharacterModel::Initialize");
        _TargetUnit = targetUnit;
        _PlayerType = playerType;
        _Character = character;
        _MaxHP = _Character._MaxHP;
        _MaxGage = _Character._MaxGage;

        _LatestLocalPos = _InitialLocalPos = localPos;
        
        _RxHP = new IntReactiveProperty(_MaxHP);
        _RxLocalPos = new Vector2ReactiveProperty(_InitialLocalPos);
        _RxShotGage = new IntReactiveProperty(_Character._InitialGage);
    }

    public void InitializeShot(Entity.Constant.PlayerType playerType, Entity.CharacterData character)
    {

    }

    /// <summary>
    /// ショットゲージを設定します。
    /// </summary>
    /// <param name="value"></param>
    public void SetCurrentShotGage(int value)
    {
        _RxShotGage.Value = Mathf.Clamp(value, 0, _MaxGage);
    }

    /// <summary>
    /// ショットゲージを加算します。
    /// </summary>
    /// <param name="value"></param>
    public void AddShotGage(int value) => SetCurrentShotGage(ShotGage + value);

    /// <summary>
    /// HPを設定します。
    /// </summary>
    /// <param name="value"></param>
    public void SetCurrentHP(int value)
    {
        _RxHP.Value = Mathf.Clamp(value, 0, _MaxHP);
    }

    /// <summary>
    /// HPを加算します。
    /// </summary>
    /// <param name="value"></param>
    public void AddCurrentHP(int value) => SetCurrentHP(HP + value);

    /// <summary>
    /// ローカルポジションを設定します。
    /// </summary>
    /// <param name="value"></param>
    public void SetLocalPos(Vector2 value) => LocalPos = value;

    public void AddLoadPos(Vector2 value) => SetLocalPos(LocalPos + (value * _Character._MoveSpeed));

    public Entity.ShotData GetShot01() => _Character._Shot1;
    public Entity.ShotData GetShot02() => _Character._Shot2;
    public Entity.ShotData GetShot03() => _Character._Shot3;
    public Entity.ShotData GetShot04() => _Character._Shot4;

    public float GetHpRate()
    {
        return (float)HP / (float)_MaxHP;
    }
}
