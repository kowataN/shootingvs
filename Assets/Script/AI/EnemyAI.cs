﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour
{
	static public readonly float[,] CheckPosition = new float[9,4]
	{
		// 960*840
		{ -2.2f, 2.6f, -0.7f, 1.3f }, { -0.7f, 2.6f, 0.7f, 1.3f }, { 0.7f, 2.6f, 2.2f, 1.3f },
		{ -2.2f, 1.3f, -0.7f, 0.3f }, {  -0.7f, 1.3f, 0.7f, 0.3f }, { 0.7f, 1.3f, 2.2f, 0.3f },
		{ -2.2f, 0.3f, -0.7f, -1.25f }, { -0.7f, 0.3f, 0.7f, -1.25f }, { 0.7f, 0.3f, 2.2f, -1.25f },
	};

	[SerializeField] private CharacterPresenter _Presenter = default;

	static public Entity.Constant.RangeType GetRange(float range)
	{
		if (range >= 3.0f)
		{
			return Entity.Constant.RangeType.kRangeLong;
		}
		if (range >= 1.0f)
		{
			return Entity.Constant.RangeType.kRangeMiddle;
		}
		return Entity.Constant.RangeType.kRangeShort;
	}

	private float _TimeLeftMove = 0.0f;
	private float _TimeLeftAttack = 0.0f;
	float _MoveX = 0.0f, _MoveY = 0.0f;
	private void Update()
    {
		if (BattleManager.Instance.BattleEndType != Entity.Constant.BattleEndType.kNone)
        {
			return;
        }

        if (_Presenter.IsMove)
        {
			// 一定間隔で移動
			_TimeLeftMove -= Time.deltaTime;
			if (_TimeLeftMove <= 0.0f)
            {
				_TimeLeftMove = 1.0f;
				var tile = GetPositionTile();
				DebugView.Instance.SetWorldPos(tile);

				GetMovePosition(_Presenter.Model.LocalPos,
					_Presenter.Model.TargetUnit.transform.position,
					tile, out _MoveX, out _MoveY);

			}
			_Presenter.Model.AddLoadPos(new Vector2(_MoveX, _MoveY) * Time.deltaTime);
		}

		if (_Presenter.IsAttack)
        {
			// 一定間隔で攻撃
			_TimeLeftAttack -= Time.deltaTime;
			if (_TimeLeftAttack <= 0.0f)
            {
				_TimeLeftAttack = 1.0f;

				Entity.ShotData shotData = default;
				if (Random.Range(0, 20) == 0 && _Presenter.IsShot04Used())
                {
					shotData = _Presenter.Model.GetShot04();
				}
				else if (Random.Range(0, 10) == 0 && _Presenter.IsShot03Used())
				{
					shotData = _Presenter.Model.GetShot03();
				}
				else if (Random.Range(0, 5) == 0 && _Presenter.IsShot02Used())
				{
					shotData = _Presenter.Model.GetShot02();
				}
				else
				{
					shotData = _Presenter.Model.GetShot01();
				}

				_Presenter.Model.AddShotGage(-shotData._UseGage);
				ShotManager.Instance.CreateShot(shotData, _Presenter.View, _Presenter.Model.TargetUnit);
			}
		}
    }

	private Entity.Constant.PositionTile GetPositionTile()
    {
		Vector2 pos = _Presenter.Model.LocalPos;
		int length = CheckPosition.Length / 4;
		for (int i=0; i<length; ++i)
        {
			if (CheckPosition[i,0] <= pos.x && pos.x < CheckPosition[i, 2]
				&& CheckPosition[i, 1] >= pos.y && pos.y > CheckPosition[i, 3])

			{
				//LogView.Log("CheckPos : " + i.ToString());
				return (Entity.Constant.PositionTile)i;
            }
        }
		return Entity.Constant.PositionTile.kPosCenter;
	}

	private void GetMovePosition(Vector2 myPos, Vector2 targetPos,
		Entity.Constant.PositionTile tile, out float moveX, out float moveY)
    {
		Vector2 targetDiff = myPos - targetPos;
		var range = GetRange(targetDiff.magnitude);
		moveX = moveY = 0.0f;
        switch (tile)
        {
			case Entity.Constant.PositionTile.kPosLeftUp:
			case Entity.Constant.PositionTile.kPosCenterUp:
			case Entity.Constant.PositionTile.kPosRightUp:
				if (range == Entity.Constant.RangeType.kRangeShort)
				{
					// 近いので相手との位置関係で移動方向決定
					moveX = myPos.x <= targetPos.x ? -1f : 1f;
					moveY = 1;
				}
				else
				{
					// 距離があるので自由に移動
					moveX = Random.Range(0, 2) == 0 ? 1f : -1f;
					moveY = Random.Range(0, 2) == 0 ? 1f : -1f;
				}
				break;

			case Entity.Constant.PositionTile.kPosLeft:
			case Entity.Constant.PositionTile.kPosCenter:
			case Entity.Constant.PositionTile.kPosRight:
				if (range == Entity.Constant.RangeType.kRangeShort)
				{
					// 近いので相手との位置関係で移動方向決定
					moveX = myPos.x <= targetPos.x ? -1f : 1f;
				}
				else
				{
					// 距離があるので自由に移動
					moveX = Random.Range(0, 2) == 0 ? 1f : -1f;
				}

				moveY = Random.Range(0, 2) == 0 ? 1f : -1f;
				break;

			case Entity.Constant.PositionTile.kPosLeftDown:
			case Entity.Constant.PositionTile.kPosCenterDown:
			case Entity.Constant.PositionTile.kPosRightDown:
				if (range == Entity.Constant.RangeType.kRangeShort)
				{
					// 近いので相手との位置関係で移動方向決定
					moveX = myPos.x <= targetPos.x ? -1f : 1f;
					moveY = -1f;
				}
				else
				{
					// 距離があるので自由に移動
					moveX = Random.Range(0, 2) == 0 ? 1f : -1f;
					moveY = Random.Range(0, 2) == 0 ? 1f : -1f;
				}
				break;

			default:
				break;
		}

		moveX *= 0.2f;
		moveY *= 0.2f;
    }
}
