﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BehaviorTree
{
    /// <summary>
    /// 基底クラス
    /// </summary>
    public abstract class Node
    {
        // 通し番号
        private int _Index = -1;
        public int Index
        {
            get { return _Index; }
            set { _Index = value; }
        }

        /// <summary>
        /// 親ノード
        /// </summary>
        protected Node _ParentNode = default;
        public Node ParentNode
        {
            get { return _ParentNode; }
            set { _ParentNode = value; }
        }

        protected Entity.Constant.BehaviorStatus _Status = default;
        public Entity.Constant.BehaviorStatus Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

        protected string _Name = default;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        /// <summary>
        /// 起動時に呼ばれる
        /// </summary>
        public virtual void OnAwake()
        {
        }

        /// <summary>
        /// 実行時に呼ばれる
        /// </summary>
        public virtual void OnStart()
        {
            Status = Entity.Constant.BehaviorStatus.kRunning;
        }

        /// <summary>
        /// 毎フレーム呼ばれる
        /// </summary>
        /// <returns></returns>
        public virtual Entity.Constant.BehaviorStatus OnUpdate()
        {
            if (Status == Entity.Constant.BehaviorStatus.kCompleted)
            {
                return Status;
            }

            if (Status == Entity.Constant.BehaviorStatus.kInactive)
            {
                OnStart();
            }

            return Status;
        }

        /// <summary>
        /// 終了時に呼ばれる
        /// </summary>
        public virtual void OnEnd()
        {
            if (Status == Entity.Constant.BehaviorStatus.kCompleted)
            {
                return;
            }

            Status = Entity.Constant.BehaviorStatus.kInactive;
        }

        public virtual void OnAbort()
        {
            OnEnd();
        }

        public virtual void AddNode(Node node)
        {
        }

        public virtual void AddNodes(params Node[] nodes)
        {

        }
    }
}
