﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugView : MonoBehaviour
{
    public static DebugView Instance;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    [SerializeField] private Text _TextRange = default;
    [SerializeField] private Text _TextRangeStr = default;
    [SerializeField] private Text _TextPosition = default;
    [SerializeField] private Text _TextWorldPos = default;
    [SerializeField] private Text _TextState = default;

    public void SetRangeText(float range)
    {
        _TextRange.text = range.ToString();
        var rangeConst = EnemyAI.GetRange(range);
        if (rangeConst == Entity.Constant.RangeType.kRangeShort)
        {
            _TextRangeStr.text = "近距離";
        } 
        else if (rangeConst == Entity.Constant.RangeType.kRangeMiddle)
        {
            _TextRangeStr.text = "中距離";
        }
        else
        {
            _TextRangeStr.text = "遠距離";
        }
    }

    public void SetPosition(Vector2 pos) => _TextPosition.text = pos.ToString();
    public void SetWorldPos(Entity.Constant.PositionTile tile) => _TextWorldPos.text = tile.ToString();
    public void SetStateStr(string stateName)
    {
        if (_TextState == null)
        {
            return;
        }

        if (_TextState.text != stateName)
        {
            LogView.Log("STATE: " + _TextState.text + " → " + stateName);
        }
        _TextState.text = stateName;
    }
}
