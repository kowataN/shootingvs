﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitHitAreaView : MonoBehaviour
{
    [SerializeField] private SpriteRenderer _Sprite = default;

    void Start()
    {
        _Sprite.enabled = false;
    }

    public void SetEnabledArea(bool value) => _Sprite.enabled = value;
}
