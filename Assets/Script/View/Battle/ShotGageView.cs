﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShotGageView : MonoBehaviour
{
    [SerializeField] private Text _GageText;
     public Text GageText 
    {
        get { return _GageText; }
        set { _GageText = value; } 
    }
}
