﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerView : MonoBehaviour
{
    [SerializeField] private Text _TextTimer = default;

    public Text TextTimer {
        get { return _TextTimer; }
        set { _TextTimer = value; }
    }
}
