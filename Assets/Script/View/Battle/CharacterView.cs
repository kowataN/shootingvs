﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterView : MonoBehaviour
{
    [SerializeField] private Transform _Transform = default;
    [SerializeField] private Transform _ShotPos = default;
    [SerializeField] private SpriteRenderer _SR = default;

    public SpriteRenderer SR { get { return _SR; } }

    public Transform ShotPos { get { return _ShotPos; } }

    public void UpdateLocalPos(Vector2 value) => _Transform.localPosition = value;

    public void UpdateDirection(Vector2 diff)
    {
        _Transform.rotation = Quaternion.FromToRotation(Vector2.up, diff);
    }
}
