﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HPGageView : MonoBehaviour
{
    [SerializeField] private Image _ImageBar = default;

    public void UpdateBar(float value)
    {
        _ImageBar.fillAmount = value;
    }
}
