﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KasuriView : MonoBehaviour
{
    [SerializeField] private CircleCollider2D _Collider = default;

    public void SetEnabled(bool value) => _Collider.enabled = value;
}
