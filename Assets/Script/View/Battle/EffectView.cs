﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EffectView : MonoBehaviour
{
    [SerializeField] Animator _Animator = default;
    [SerializeField] Text _EndText = default;

    public Action<bool> EndCallback = default;

    private void Awake()
    {
    }

    public void PlayBattleStart()
    {
        this.gameObject.SetActive(true);
        _Animator.Play("BattleStart", 0, 0.0f);
    }

    public void PlayBattleEnd()
    {
        this.gameObject.SetActive(true);
        _EndText.text = "バトル終了：" +
        (BattleManager.Instance.BattleEndType == Entity.Constant.BattleEndType.kDraw ? "引き分け" :
        BattleManager.Instance.BattleEndType == Entity.Constant.BattleEndType.kPlayerWin ? "プレイヤー勝利" : "CPU勝利");
        _Animator.Play("BattleEnd", 0, 0.0f);
    }

    public void EndAnimation()
    {
        this.gameObject.SetActive(false);
        EndCallback?.Invoke(true);
    }
}
