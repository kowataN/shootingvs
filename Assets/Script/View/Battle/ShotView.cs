﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotView : MonoBehaviour
{
    [SerializeField] private Transform _Transform = default;
    public Transform Transform { get; set; }

    public void UpdateLocalPos(Vector2 value) => _Transform.localPosition = value;
}
