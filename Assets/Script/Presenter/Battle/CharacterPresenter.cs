﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UniRx.Triggers;
using System;

public class CharacterPresenter : MonoBehaviour
{
    private CharacterModel _Model = default;
    public CharacterModel Model { get { return _Model; } }

    [SerializeField] private CharacterView _View = default;
    public CharacterView View { get { return _View; } }

    [SerializeField] private KasuriPresenter _KasuriPresenter = default;
    [SerializeField] private UnitHitAreaPresenter _UnitHitAreaPresenter = default;

    private static readonly float _DeltaTime = 0.0333f;
    private bool _IsBlink = false;

    [Header("AI用")]
    [SerializeField,Tooltip("移動フラグ ON:移動、OFF:静止")] private bool _IsMove = false;
    public bool IsMove 
    {
        get { return _IsMove; }
        set { _IsMove = value; }
    }

    [SerializeField] private bool _IsAttack = false;
    public bool IsAttack
    {
        get { return _IsAttack; }
        set { _IsAttack = value; }
    }

    public void Initialize(CharacterModel model)
    {
        _Model = model;
        SetObservable();

        _KasuriPresenter?.Initialize(model);
        _UnitHitAreaPresenter?.Initialize(model);
    }

    private void SetObservable()
    {
        if (_Model.IsPlayer)
        {
            // 方向キー入力
            this.UpdateAsObservable()
                .Subscribe(_ =>
                {
                    float addX = Input.GetAxisRaw("Horizontal");
                    float addY = Input.GetAxisRaw("Vertical");
                    if (addX == 0 && addY == 0)
                    {
                        _Model.LatestLocalPos = _Model.LocalPos;
                    }
                    else
                    {
                        _Model.AddLoadPos(new Vector2(addX, addY) * Time.deltaTime);
                    }
                }).AddTo(_View.gameObject);
        }

        // キャラの向き変更
        this.UpdateAsObservable()
            .Subscribe(_ => {
                Vector2 targetDiff = _Model.TargetUnit.transform.localPosition;
                targetDiff -= _Model.LocalPos;
                if (targetDiff.magnitude > 0.01f)
                {
                    _View.UpdateDirection(targetDiff);
                    if (_Model.IsPlayer == false)
                    {
                        DebugView.Instance.SetRangeText(targetDiff.magnitude);
                    }
                }
            }).AddTo(_View.gameObject);

        // HPが減少したら点滅
        _Model.RxHP.ObserveEveryValueChanged(x => x.Value)
            .Where(x => x < _Model.MaxHP)
            .Subscribe(_ => {
                if (_IsBlink) { return; }
                _IsBlink = true;
                StartCoroutine("WaitForBlink");
            }).AddTo(_View.gameObject);

        // HPが0以下になったら終了フラグを立てる
        _Model.RxHP.ObserveEveryValueChanged(x => x.Value)
            .Where(x => x <= 0)
            .Subscribe( _ => {
                BattleManager.Instance.UnitDead(_Model.IsPlayer);
            }).AddTo(_View.gameObject);

        Observable.Interval(TimeSpan.FromSeconds(_DeltaTime))
            .Where(_ => _IsBlink)
            .Subscribe(_ => {
                float level = Mathf.Abs(Mathf.Sin(Time.time * 10));
                _View.SR.color = new Color(1f, 1f, 1f, level);
            }).AddTo(_View.gameObject);

        // 弾発射
        if (_Model.IsPlayer)
        {
            #region 通常弾
            var normalAtkDownStream = this.UpdateAsObservable().Where(_ => Input.GetKeyDown(KeyCode.Z));
            var normalAtkUpStream = this.UpdateAsObservable().Where(_ => Input.GetKeyUp(KeyCode.Z));

            // 通常攻撃
            this.UpdateAsObservable()
                .Where(_ => Input.GetKeyDown(KeyCode.Z))
                .Subscribe(_ => {
                    Entity.ShotData shotData = _Model.GetShot01();
                    ShotManager.Instance.CreateShot(shotData, _View, _Model.TargetUnit);
                }).AddTo(_View.gameObject);

            // 通常攻撃長押し
            normalAtkDownStream
                .SelectMany(_ => Observable.Interval(TimeSpan.FromSeconds(0.5f)))
                .TakeUntil(normalAtkUpStream)
                .RepeatUntilDestroy(_View.gameObject)
                .Subscribe(_ => {
                    //LogView.Log("長押し");
                    Entity.ShotData shotData = _Model.GetShot04();
                    ShotManager.Instance.CreateShot(shotData, _View, _Model.TargetUnit);
                });

            // 長押しキャンセル
            normalAtkUpStream.Timestamp()
                .Zip(normalAtkUpStream.Timestamp(), (d, u) => (u.Timestamp - d.Timestamp).TotalMilliseconds / 1000.0f)
                .Where(Time => Time < 1.0f)
                .Subscribe();

            #endregion

            #region 特殊弾1
            this.UpdateAsObservable()
                .Where(_ => Input.GetKeyDown(KeyCode.X))
                .Subscribe(_ => {
                    var shotData = _Model.GetShot02();
                    if (_Model.ShotGage < shotData._UseGage)
                    {
                        return;
                    }
                    _Model.AddShotGage(-shotData._UseGage);
                    ShotManager.Instance.CreateShot(shotData, _View, _Model.TargetUnit);
                }).AddTo(_View.gameObject);
            #endregion

            #region 特殊弾2
            this.UpdateAsObservable()
                .Where(_ => Input.GetKeyDown(KeyCode.C))
                .Subscribe(_ => {
                    Entity.ShotData shotData = _Model.GetShot03();
                    if (_Model.ShotGage < shotData._UseGage)
                    {
                        return;
                    }
                    _Model.AddShotGage(-shotData._UseGage);
                    ShotManager.Instance.CreateShot(shotData, _View, _Model.TargetUnit);
                }).AddTo(_View.gameObject);
            #endregion
        }


        // キャラの移動
        _Model.LocalPosObservable
            .Subscribe(x => _View.UpdateLocalPos(x))
            .AddTo(_View.gameObject);
    }

    IEnumerator WaitForBlink()
    {
        // 1秒間処理を止める
        yield return new WaitForSeconds(1);

        // １秒後ダメージフラグをfalseにして点滅を戻す
        _IsBlink = false;
        _View.SR.color = new Color(1f, 1f, 1f, 1f);
    }

    public bool IsShot02Used()
    {
        return _Model.GetShot02()._UseGage <= _Model.ShotGage;
    }
    public bool IsShot03Used()
    {
        return _Model.GetShot03()._UseGage <= _Model.ShotGage;
    }
    public bool IsShot04Used()
    {
        return _Model.GetShot04()._UseGage <= _Model.ShotGage;
    }
}

