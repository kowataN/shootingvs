﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class TimerPresenter : MonoBehaviour
{
    [SerializeField] private TimerView _View = default;
    private TimerModel _Model = default;

    public void Initialize(int time)
    {
        _Model = new TimerModel();
        _Model.Initialize(time, gameObject);

        SetObservable();
    }

    private void SetObservable()
    {
        // 毎秒減算
        Observable.Timer(TimeSpan.FromSeconds(_Model.Interval),
                         TimeSpan.FromSeconds(_Model.Interval))
                  .Where(_ => _Model.TimerState == TimerModel.TimerStateType.Loop 
                            && BattleManager.Instance.BattleEndType == Entity.Constant.BattleEndType.kNone)
                  .Subscribe(_ => {
                      _Model.Second.Value = Mathf.Clamp(--_Model.Second.Value, 0, _Model.Second.Value);
                  }).AddTo(_View.gameObject);

        _Model.Second.AsObservable()
            .Where(x => x <= 0)
            .Subscribe(_ => {
                
            }).AddTo(_View.gameObject);

        // 画面更新
        _Model.Second.SubscribeToText(_View.TextTimer);
    }
}
