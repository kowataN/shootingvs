﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UniRx.Triggers;
using System;

public class ShotPresenter : MonoBehaviour
{
    [SerializeField] private ShotView _View = default;
    [SerializeField] private ShotModel _Model = default;

    /// <summary>
    /// 経過フレーム数
    /// </summary>
    private float _ProgessFrame = default;
    private float _StartFrame = default;

    public void Initialize(Entity.ShotData shotData, GameObject targetUnit, Vector2 objPos)
    {
        SetTag(targetUnit.tag);
        _Model.Initialize(shotData, targetUnit, objPos);
        _Model.SetRot(targetUnit.transform.localPosition);
        SetObservable();
    }

    public void InitializeWide(Entity.WideShotData shotData, GameObject targetUnit, Vector2 objPos, float addRot)
    {
        SetTag(targetUnit.tag);
        _Model.Initialize(shotData, targetUnit, objPos);
        _Model.SetRotWide(targetUnit.transform.localPosition, addRot);
        SetObservable();
    }

    public void InitializeHoming(Entity.HomingShotData shotData, GameObject targetUnit, Vector2 objPos)
    {
        SetTag(targetUnit.tag);
        _Model.Initialize(shotData, targetUnit, objPos);
        _Model.SetRot(targetUnit.transform.localPosition);
        _StartFrame = Time.frameCount;
        SetObservableHoming();
    }

    private void SetTag(string tag)
    {
        this.tag = tag == "EnemyShip" ? "PlayerBullet" : "EnemyBullet";
    }

    private void SetObservable()
    {
        // 移動
        this.UpdateAsObservable()
            .Subscribe(_ => {
                _Model.UpdatLocalPos();
                if (_Model.LocalPos.x < -4.0f || _Model.LocalPos.x > 4.0f
                    || _Model.LocalPos.y < -2.0f || _Model.LocalPos.y > 4.0f)
                {
                    Destroy(_View.gameObject);
                }
            }).AddTo(_View.gameObject);


        // 画面に反映
        _Model.LocalPosObservable
            .Subscribe(x => _View.UpdateLocalPos(x))
            .AddTo(_View.gameObject);

        // 回転
        this.UpdateAsObservable()
            .Where(_ => _Model.ShotData._Type == Entity.Constant.ShotType.kNormalPush)
            .Subscribe(_ => {
                var angle = _View.transform.eulerAngles;
                angle.z += 5.0f;
                _View.transform.eulerAngles = angle;
            })
            .AddTo(_View.gameObject);
    }

    private void SetObservableHoming()
    {
        // 移動
        this.UpdateAsObservable()
            .Subscribe(_ => {
                _ProgessFrame = Time.frameCount - _StartFrame;
                Entity.HomingShotData homingData = (Entity.HomingShotData)_Model.ShotData;
                if (homingData._StartHomingFrame <= _ProgessFrame)
                {
                    homingData._State = Entity.HomingShotData.HomingState.kHoming;
                }
                if (homingData._StopHomingFrame <= _ProgessFrame)
                {
                    homingData._State = Entity.HomingShotData.HomingState.kStop;
                }

                if (homingData._State == Entity.HomingShotData.HomingState.kHoming)
                {
                    bool isInterval = _ProgessFrame % homingData._HomingInterval == 0;
                    if (isInterval)
                    {
                        _Model.SetRotHoming(_Model.TargetUnit.transform, homingData._AddRotDeg, _View.transform);
                    }
                }

                _Model.UpdatLocalPos();
                if (_Model.LocalPos.x < -4.0f || _Model.LocalPos.x > 4.0f
                    || _Model.LocalPos.y < -2.0f || _Model.LocalPos.y > 4.0f)
                {
                    Destroy(_View.gameObject);
                }
            }).AddTo(_View.gameObject);

        // 画面に反映
        _Model.LocalPosObservable
            .Subscribe(x => _View.UpdateLocalPos(x))
            .AddTo(_View.gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == _Model.TargetTag)
        {
            BattleManager.Instance.UpdateHpBar(_Model.TargetTag, _Model.ShotData._Attack);
            Destroy(gameObject);
        }
    }
}
