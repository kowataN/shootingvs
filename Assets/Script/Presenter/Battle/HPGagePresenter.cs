﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UniRx.Triggers;

public class HPGagePresenter : MonoBehaviour
{
    private CharacterModel _Model = default;
    [SerializeField] private HPGageView _View = default;

    public void Initialize(CharacterPresenter presenter, GameObject targetObj)
    {
        _Model = presenter.Model;

        SetObservable(targetObj);
    }

    private void SetObservable(GameObject targetObj)
    {
        _Model.RxHP.ObserveEveryValueChanged(x => x.Value)
            .Subscribe(x => {
                float value = _Model.GetHpRate();
                _View.UpdateBar(value);
            })
            .AddTo(targetObj);
    }
}
