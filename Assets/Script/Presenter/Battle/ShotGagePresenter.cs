﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class ShotGagePresenter : MonoBehaviour
{
    private CharacterModel _Model = default;
    [SerializeField] private ShotGageView _View = default;

    public void Initialize(CharacterPresenter presenter, GameObject targetObj)
    {
        _Model = presenter.Model;

        SetObservable(targetObj);
    }

    private void SetObservable(GameObject targetObj)
    {
        _Model.RxShotGage
            .ObserveEveryValueChanged(x => x.Value)
            .SubscribeToText(_View.GageText)
            .AddTo(targetObj);
    }
}
