﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitHitAreaPresenter : MonoBehaviour
{
    protected CharacterModel _Model = default;
    [SerializeField] private UnitHitAreaView _View = default;

    public void Initialize(CharacterModel model)
    {
        _Model = model;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == Entity.TagName.UnitHitArea)
        {
            _View.SetEnabledArea(true);
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == Entity.TagName.UnitHitArea)
        {
            var Unit1Pos = _Model.LocalPos;
            var Unit2Pos = _Model.TargetUnit.transform.localPosition;
            if (Unit1Pos.x <= Unit2Pos.x)
            {
                _Model.AddLoadPos(new Vector2(-0.01f, 0));
            }
            else
            {
                _Model.AddLoadPos(new Vector2(0.01f, 0));
            }

            if (Unit1Pos.y <= Unit2Pos.y)
            {
                _Model.AddLoadPos(new Vector2(0.0f, -0.01f));
            }
            else
            {
                _Model.AddLoadPos(new Vector2(0.0f, 0.01f));
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == Entity.TagName.UnitHitArea)
        {
            _View.SetEnabledArea(false);
        }
    }
}
