﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace State
{
    public class StateEndFadeOut : StateBase
    {
        private SubState _SubState = default;

        public StateEndFadeOut(executeState execute) : base(execute)
        {
            _SubState = new SubState(ExecuteInit);
        }

        public override void Execute()
        {
            _SubState.Execute();
            if (ExecDelegate != null)
            {
                ExecDelegate();
            }
        }

        private void ExecuteInit()
        {
            Fader.Instance.BlackOut(2.0f, ExecuteFadeOutEnd);
            _SubState.ChangeState(ExecuteIdle);
        }

        private void ExecuteIdle()
        {

        }

        private void ExecuteFadeOutEnd()
        {
            EndFlag = true;
        }
    }
}
