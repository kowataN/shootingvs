﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace State
{
    public class StateBattleStartEffect : StateBase
    {
        private SubState _SubState = default;

        public StateBattleStartEffect(executeState execute) : base(execute)
        {
            _SubState = new SubState(ExecuteInit);
        }

        public override void Execute()
        {
            _SubState.Execute();
            if (ExecDelegate != null)
            {
                ExecDelegate();
            }
        }

        private void ExecuteInit()
        {
            BattleManager.Instance.EffectView.EndCallback += ExecuteEnd;
            BattleManager.Instance.EffectView.PlayBattleStart();

            _SubState.ChangeState(ExecuteIdle);
        }

        private void ExecuteIdle()
        {

        }

        private void ExecuteEnd(bool endflag)
        {
            BattleManager.Instance.EffectView.EndCallback -= ExecuteEnd;
            EndFlag = true;
        }
    }
}
