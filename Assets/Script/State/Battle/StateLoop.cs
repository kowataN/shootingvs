﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace State
{
    public class StateLoop : StateBase
    {
        private SubState _SubState = default;

        public StateLoop(executeState execute) : base(execute)
        {
            _SubState = new SubState(ExecuteInit);
        }

        public override void Execute()
        {
            _SubState.Execute();
            if (ExecDelegate != null)
            {
                ExecDelegate();
            }
        }

        private void ExecuteInit()
        {
            BattleManager.Instance.UpdateEnemyAction(true, true);
            _SubState.ChangeState(ExecuteLoop);
        }

        private void ExecuteLoop()
        {
            if (BattleManager.Instance.BattleEndType != Entity.Constant.BattleEndType.kNone)
            {
                EndFlag = true;
            }
        }
    }
}
