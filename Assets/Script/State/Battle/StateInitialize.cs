﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace State
{
    public class StateInitialize : StateBase
    {
        private SubState _SubState = default;

        public StateInitialize(executeState execute) : base(execute)
        {
            _SubState = new SubState(ExecuteInit);
        }

        public override void Execute()
        {
            _SubState.Execute();
            if (ExecDelegate != null)
            {
                ExecDelegate();
            }
        }

        private void ExecuteInit()
        {
            Fader.Instance.SetColorAndActive(Color.black, true);

            // 初期化処理
            BattleManager.Instance.Initialize();

            EndFlag = true;
        }
    }
}
