﻿using UnityEngine;

namespace Entity
{
    [CreateAssetMenu(fileName = "Data", menuName = "Entity/CreateCharater")]
    public class CharacterData : ScriptableObject
    {
        public int _ID = 1;
        public string _Name = "ユニット";

        [Header("最大値")]
        public int _MaxHP = 1000;
        public int _MaxGage = 500;

        [Header("初期値")]
        public int _InitialGage = 250;

        [Header("移動速度")]
        public float _MoveSpeed = 3.0f;

        [Header("通常弾")]
        public Entity.ShotData _Shot1;
        [Header("特殊弾1")]
        public Entity.ShotData _Shot2;
        [Header("特殊弾2")]
        public Entity.ShotData _Shot3;

        [Header("通常弾長押し")]
        public Entity.ShotData _Shot4;
    }
}
