﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Entity
{
    public class Constant
    {
        public enum PlayerType {
            kUser, kAI
        }

        public enum ShotType
        {
            kNormal, kWide, kHoming, kBomb, kNormalPush
        }

        public enum BehaviorStatus
        {
            kInactive, kSuccess, kFailure, kRunning, kCompleted
        }

        //	位置
        public enum PositionTile
        {
            kPosLeftUp,     //	左上
            kPosCenterUp,   //	上
            kPosRightUp,    //	右上
            kPosLeft,       //	左
            kPosCenter,     //	真ん中
            kPosRight,      //	右
            kPosLeftDown,   //	左下
            kPosCenterDown, //	下
            kPosRightDown,  //	右下
            kPosMax,        //	最大値
        };

        //	距離
        public enum RangeType
        {
            kRangeShort,
            kRangeMiddle,
            kRangeLong,
        };

        public enum BattleEndType
        {
            kNone,
            kPlayerWin,
            kCPUWin,
            kDraw
        };
    }
}

