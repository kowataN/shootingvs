﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UniRx.Triggers;

public class BattleManager : SingletonMonoBhv<BattleManager>
{
    [SerializeField] GameObject PlayerPrefab = default;
    [SerializeField] GameObject EnemyPrefab = default;

    [SerializeField] private Transform _CharacterPos = default;
    private GameObject _Player = default;
    private GameObject _Enemy = default;

    private CharacterPresenter _PlayerPresenter = default;
    private CharacterPresenter _EnemyPresenter = default;

    [SerializeField] private HPGagePresenter _PlayerHpGagePresenter = default;
    [SerializeField] private HPGagePresenter _EnemyHpGagePresenter = default;

    [SerializeField] private ShotGagePresenter _PlayerShotGagePresenter = default;
    [SerializeField] private ShotGagePresenter _EnemyShotGagePresenter = default;

    [SerializeField] private int Timer = 99;
    [SerializeField] private TimerPresenter _TimerPresenter = default;

    [SerializeField] private EffectView _EffectView = default;
    public EffectView EffectView
    {
        get { return _EffectView; }
    }

    [SerializeField] private Entity.Constant.BattleEndType _BattleEndType = Entity.Constant.BattleEndType.kNone;
    public Entity.Constant.BattleEndType BattleEndType
    {
        get { return _BattleEndType; }
        set { _BattleEndType = value; }
    }


    private void Awake()
    {
        BattleEndType = Entity.Constant.BattleEndType.kNone;
    }

    private void Start()
    {
        StateManager.Instance.Initialize();
        this.UpdateAsObservable()
            .Subscribe(_ =>
            {
                StateManager.Instance.Execute();
            })
            .AddTo(this.gameObject);
    }

    public void Initialize()
    {
        if (_Player != null)
        {
            Destroy(_Player);
        }
        if (_Enemy != null)
        {
            Destroy(_Enemy);
        }

        _Player = GameObject.Instantiate<GameObject>(PlayerPrefab);
        _Player.transform.parent = _CharacterPos;
        _Enemy = GameObject.Instantiate<GameObject>(EnemyPrefab);
        _Enemy.transform.parent = _CharacterPos;

        // プレイヤー初期化
        _PlayerPresenter = _Player.GetComponentInChildren<CharacterPresenter>();
        CharacterModel playerModel = new CharacterModel();
        Entity.CharacterData playerData = Resources.Load<Entity.CharacterData>("SO/Character1");
        playerModel.Initialize(Entity.Constant.PlayerType.kUser, playerData,
            _Player.transform.localPosition, _Enemy);

        _PlayerPresenter.Initialize(playerModel);
        _PlayerHpGagePresenter?.Initialize(_PlayerPresenter, _Player);
        _PlayerShotGagePresenter?.Initialize(_PlayerPresenter, _Player);


        // エネミー初期化
        _EnemyPresenter = _Enemy.GetComponentInChildren<CharacterPresenter>();
        CharacterModel enemyModel = new CharacterModel();
        Entity.CharacterData enemyData = Resources.Load<Entity.CharacterData>("SO/Character2");
        enemyModel.Initialize(Entity.Constant.PlayerType.kAI, enemyData,
            _Enemy.transform.localPosition, _Player);
        enemyModel.SetLocalPos(new Vector2(0.0f, 2.5f));

        _EnemyPresenter.Initialize(enemyModel);
        _EnemyHpGagePresenter?.Initialize(_EnemyPresenter, _Enemy);
        _EnemyShotGagePresenter?.Initialize(_EnemyPresenter, _Enemy);


        // タイマー初期化
        _TimerPresenter?.Initialize(Timer);
    }

    public void UpdateHpBar(string targetTag, int damage)
    {
        if (targetTag == "PlayerShip")
        {
            _PlayerPresenter.Model.AddCurrentHP(-damage);
        }
        else
        {
            _EnemyPresenter.Model.AddCurrentHP(-damage);
        }
    }

    public void UpdateEnemyAction(bool isMove, bool isAttack)
    {
        _EnemyPresenter.IsMove = isMove;
        _EnemyPresenter.IsAttack = isAttack;
    }

    public void UnitDead(bool isPlayer)
    {
        BattleEndType = isPlayer ? Entity.Constant.BattleEndType.kCPUWin
            : Entity.Constant.BattleEndType.kPlayerWin;
    }

    public void CheckBattkeEnd()
    {
        if (_PlayerPresenter.Model.HP <= 0 && _EnemyPresenter.Model.HP <= 0)
        {
            BattleEndType = Entity.Constant.BattleEndType.kDraw;
        }
        else
        {
            if (_PlayerPresenter.Model.HP <= 0)
            {
                BattleEndType = Entity.Constant.BattleEndType.kCPUWin;
            }
            else if (_EnemyPresenter.Model.HP <= 0)
            {
                BattleEndType = Entity.Constant.BattleEndType.kPlayerWin;
            }
        }
    }
}